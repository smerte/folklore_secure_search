#ifndef INCLUDE_HELIB_CONTEXT_HPP_
#define INCLUDE_HELIB_CONTEXT_HPP_

#include <FHE/FHE.h>
#include <FHE/EncryptedArray.h>

#include <FHE/NumbTh.h>
#include <FHE/intraSlot.h>
#include <NTL/ZZX.h>

#include <memory>
#include <string>

//	k - security parameter
//	L - number of levels
//	c - number of columns in key switching matrices
//	p - characteristic of plaintext space
//	d - embedding degree (d==0 or d==1 => no constraint, d==0 => factors[0] defines extension)
//	s - at least that many plaintext slots
//	r - lifting - Work in the Ring R_(p^r)
//  w - Hamming weight of secret key
//  m - the ciphertext modulus
struct helib_context {
	std::shared_ptr<FHEcontext> context;
	std::shared_ptr<FHESecKey> secret_key;
	std::shared_ptr<FHEPubKey> public_key;
	std::shared_ptr<EncryptedArray> encrypted_array;

//	std::shared_ptr<std::vector<zzX>> unpack_slot_encoding;

	long param_L;
	long param_p;
	long param_r;
	long param_k;
	long param_s;
	long param_c;
	long param_d;
	long param_m;
	long param_w;

	long param_extra_bits;
	bool param_bootstrap;

	/**
	 * @brief Returns smallest parameter m satisfying various constraints:
	 * @param k security parameter
	 * @param L number of levels
	 * @param c number of columns in key switching matrices
	 * @param p characteristic of plaintext space
	 * @param d embedding degree (d ==0 or d==1 => no constraint)
	 * @param chosen_s at least that many plaintext slots
	 * @param chosen_m preselected value of m (0 => not preselected)
	 * Fails with an error message if no suitable m is found
	 * prints an informative message if verbose == true
	 **/
	long find_m(long k, long L, long c, long p, long d, long chosen_s, long chosen_m, bool verbose=false) {
	  // get a lower-bound on the parameter N=phi(m):
	  // 1. Each level in the modulus chain corresponds to pSize=p2Size/2
	  //    bits (where we have one prime of this size, and all the others are of
	  //    size p2Size).
	  //    When using DoubleCRT, we need 2m to divide q-1 for every prime q.
	  // 2. With L levels, the largest modulus for "fresh ciphertexts" has size
	  //          Q0 ~ p^{L+1} ~ 2^{(L+1)*pSize}
	  // 3. We break each ciphertext into upto c digits, do each digit is as large
	  //    as    D=2^{(L+1)*pSize/c}
	  // 4. The added noise variance term from the key-switching operation is
	  //    c*N*sigma^2*D^2, and this must be mod-switched down to w*N (so it is
	  //    on par with the added noise from modulus-switching). Hence the ratio
	  //    P that we use for mod-switching must satisfy c*N*sigma^2*D^2/P^2<w*N,
	  //    or    P > sqrt(c/w) * sigma * 2^{(L+1)*pSize/c}
	  // 5. With this extra P factor, the key-switching matrices are defined
	  //    relative to a modulus of size
	  //          Q0 = q0*P ~ sqrt{c/w} sigma 2^{(L+1)*pSize*(1+1/c)}
	  // 6. To get k-bit security we need N>log(Q0/sigma)(k+110)/7.2, i.e. roughly
	  //          N > (L+1)*pSize*(1+1/c)(k+110) / 7.2

	  // Compute a bound on m, and make sure that it is not too large
	  double cc = 1.0+(1.0/(double)c);
	  double dN = ceil((L+1)*FHE_pSize*cc*(k+110)/7.2);
	  long N = NTL_SP_BOUND;
	  if (N > dN) N = dN;
	  else {
		cerr << "Cannot support a bound of " << dN;
		Error(", aborting.\n");
	  }

	  long m = 0;
	  size_t i=0;

	  // find the first m satisfying phi(m)>=N and d | ord(p) in Z_m^*
	  // and phi(m)/ord(p) >= s
	  if (chosen_m) {
		if (GCD(p, chosen_m) == 1) {
		  long ordP = multOrd(p, chosen_m);
		  if (d == 0 || ordP % d == 0) {
			// chosen_m is OK
			m = chosen_m;
		  }
		}
	  }
	  else if (p==2) { // use pre-computed table, divisors of 2^n-1 for some n's

		static long ms[][4] = {  // pre-computed values of [phi(m),m,d]
		  //phi(m), m, ord(2),c_m*1000 (not used anymore)
		  { 1176,  1247, 28,  3736}, // gens=5(42)
		  { 2880,  3133, 24,  3254}, // gens=6(60), 7(!2)
		  { 4050,  4051, 50, 0},     // gens=130(81)
		  { 4096,  4369, 16,  3422}, // gens=129(16),3(!16)
		  { 4704,  4859, 28, 0},     // gens=7(42),3(!4)
		  { 5292,  5461, 14,  4160}, // gens=3(126),509(3)
		  { 5760,  8435, 24,  8935}, // gens=58(60),1686(2),11(!2)
		  { 7500,  7781, 50, 0},     // gens=353(30),3(!5)
		  { 8190,  8191, 13,  1273}, // gens=39(630)
		  { 9900, 10261, 30, 0},     // gens=3(330)
		  {10752, 11441, 48,  3607}, // gens=7(112),5(!2)
		  {10800, 11023, 45, 0},     // gens=270(24),2264(2),3(!5)
		  {12000, 13981, 20,  2467}, // gens=10(30),23(10),3(!2)
		  {11520, 15665, 24, 14916}, // gens=6(60),177(4),7(!2)
		  {14112, 14351, 18, 0},     // gens=7(126),3(!4)
		  {15004, 15709, 22,  3867}, // gens=5(682)
		  {18000, 18631, 25,  4208}, // gens=17(120),1177(6)
		  {15360, 20485, 24, 12767}, // gens=6(80),242(4),7(2)
		  {16384, 21845, 16, 12798}, // gens=129(16),273(4),3(!16)
		  {17280 ,21931, 24, 18387}, // gens=6(60),467(6),11(!2)
		  {19200, 21607, 40, 35633}, // gens=13(120),2789(2),3(!2)
		  {21168, 27305, 28, 15407}, // gens=6(126),781(6)
		  {23040, 23377, 48,  5292}, // gens=35(240),5(!2)
		  {23310, 23311, 45, 0},     // gens=489(518)
		  {24576, 24929, 48,  5612}, // gens=12(256),5(!2)
		  {27000, 32767, 15, 20021}, // gens=3(150),873(6),6945(2)
		  {31104, 31609, 72,  5149}, // gens=11(216),5(!2)
		  {43690, 43691, 34, 0},     // gens=69(1285)
		  {49500, 49981, 30, 0},     // gens=3(1650)
		  {46080, 53261, 24, 33409}, // gens=3(240),242(4),7(!2)
		  {54000, 55831, 25, 0},     // gens=22(360),3529(6)
		  {49140, 57337, 39,  2608}, // gens=39(630),40956(2)
		  {51840, 59527, 72, 21128}, // gens=58(60),1912(6),7(!2)
		  {61680, 61681, 40,  1273}, // gens=33(771),17(!2)
		  {65536, 65537, 32,  1273}, // gens=2(32),3(!2048)
		  {75264, 82603, 56, 36484}, // gens=3(336),24294(2),7(!2)
		  {84672, 92837, 56, 38520}  // gens=18(126),1886(6),3(!2)
		};
		for (i=0; i<sizeof(ms)/sizeof(long[4]); i++) {
		  if (ms[i][0] < N || GCD(p, ms[i][1]) != 1) continue;
		  long ordP = multOrd(p, ms[i][1]);
		  long nSlots = ms[i][0]/ordP;
		  if (d != 0 && ordP % d != 0) continue;
		  if (nSlots < chosen_s) continue;

		  m = ms[i][1];
		  break;
		}
	  }

	  // If m is not set yet, just set it close to N. This may be a lousy
	  // choice of m for this p, since you will get a small number of slots.

	  if (m==0) {
		// search only for odd values of m, to make phi(m) a little closer to m
		for (long candidate=N|1; candidate<10*N; candidate+=2) {
		  if (GCD(p,candidate)!=1) continue;

		  long ordP = multOrd(p,candidate); // the multiplicative order of p mod m
		  if (d>1 && ordP%d!=0 ) continue;
		  if (ordP > 100) continue;  // order too big, we will get very few slots

		  long n = phi_N(candidate); // compute phi(m)
		  if (n < N) continue;       // phi(m) too small

		  long nSlots = n/ordP;
		  if (nSlots < chosen_s) continue;	//

		  m = candidate;  // all tests passed, return this value of m
		  break;
		}
	  }

	  if (verbose) {
		cerr << "*** Bound N="<<N<<", choosing m="<<m <<", phi(m)="<< phi_N(m)
			 << endl;
	  }

	  return m;
	}

	helib_context(bool bootstrap, long L, long p = 2, long r = 1, long k = 80,
			long c = 3, long d = 1, long w = 128,
			long chosen_s = 0, long chosen_m = 0,
			long extra_bits = 8) {

		param_L = L; param_p = p; param_r = r; param_k = k;
		param_c = c; param_d = d; param_w = w;
		param_extra_bits = extra_bits; param_bootstrap = bootstrap;

		param_m = find_m(k, L, c, p, d, chosen_s, chosen_m);

		context = std::shared_ptr<FHEcontext>(new FHEcontext(param_m, param_p, param_r));

		if (param_bootstrap) {
			buildModChain(*context, param_L, param_c, param_extra_bits);
			std::vector<long> m_factorization;
			factorize(m_factorization, param_m);
			NTL::Vec<long> mvec;
			std::copy(m_factorization.begin(), m_factorization.end(), mvec.begin());
		    context->makeBootstrappable(mvec, /*t=*/0, /*flag=*/false, /*cacheType=DCRT*/2);
		}
		else {
			buildModChain(*context, param_L, param_c);
		}

		secret_key = std::shared_ptr<FHESecKey>(new FHESecKey(*context));
		public_key = secret_key;

		secret_key->GenSecKey(param_w);
		addSome1DMatrices(*secret_key);
		addFrbMatrices(*secret_key);
		if (param_bootstrap) {
			secret_key->genRecryptData();
		}

		ZZX G;
		if (param_d == 0) {
			G = context->alMod.getFactorsOverZZ()[0];
		}
		else {
			G = makeIrredPoly(param_p, param_d);
		}

		encrypted_array = std::shared_ptr<EncryptedArray>(new EncryptedArray(*context, G));
		param_s = encrypted_array->size();

//		buildUnpackSlotEncoding(*unpack_slot_encoding, *encrypted_array);
	}

	~helib_context() {
		context = nullptr;
		secret_key = nullptr;
		public_key = nullptr;
		encrypted_array = nullptr;
	}
};

#endif /* INCLUDE_HELIB_CONTEXT_HPP_ */
