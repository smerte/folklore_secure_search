#include <FHE/FHE.h>
#include <benchmark/benchmark.h>
#include "ctxt_heap.hpp"
#include "helib_context.hpp"
#include "ad_hoc_ctxt_iterator.hpp"
#include <functional>
#include <algorithm>
#include <thread>


size_t get_hardware_thread_count() {
	const size_t default_hardware_threads = 16;
	size_t hardware_threads = std::thread::hardware_concurrency();
	return hardware_threads != 0 ? hardware_threads : default_hardware_threads;
}

#define get_bit(x, w) (((x) >> (w)) & 1)
size_t ciel_log2(size_t x) {
	size_t log2_x = std::log2(x);
	return (x - (1 << log2_x)) ? (log2_x + 1) : log2_x;
}

class folklore_search {
private:
	const EncryptedArray& _ea;
	const FHEPubKey& _public_key;
	const FHESecKey& _secret_key;
public:
	folklore_search(const EncryptedArray& ea, const FHEPubKey& public_key, const FHESecKey& secret_key) :
			_ea(ea),
			_public_key(public_key),
			_secret_key(secret_key)

		{	}


	static size_t calc_level_approximation(size_t input_size) {
		size_t log_input_suze = ciel_log2(input_size);
		return (log_input_suze * ciel_log2(log_input_suze)) + 3;
	}

	static size_t calc_output_size(size_t input_size) {
		return ciel_log2(input_size);
	}

	template<class InputIt, class OutputIt>
	void execute(InputIt first, InputIt last, OutputIt output_first) {
		size_t input_size = std::distance(first, last);
		size_t output_size = ciel_log2(input_size);

		std::vector<ctxt_heap_sum> output_heaps = std::vector<ctxt_heap_sum>(output_size);
		ctxt_heap_product product_heap;

		InputIt i = first;
		size_t index = 0;
		{
			Ctxt tmp(ZeroCtxtLike, *i);
			tmp.addConstant(ZZ(1));
			tmp.addCtxt(*first, true);
			product_heap.store(tmp);
		}

		++i;
		++index;
		while (i != last) {
			Ctxt tmp_product(ZeroCtxtLike, *i);
			product_heap.extract_result(tmp_product);

			//cout <<"[" << i << "]\ttmp_product.findBaseLevel = " << tmp_product.findBaseLevel() << endl;

			for (size_t bit = 0; bit < output_size; bit++) {
				Ctxt bit_ctxt(ZeroCtxtLike, *i);
				bit_ctxt = tmp_product;
				if (get_bit(index, bit)) {
					bit_ctxt.multiplyBy(*i);
					output_heaps[bit].store(bit_ctxt);
					//cout <<"[" << i << "][" << bit <<"]\tbit_ctxt.findBaseLevel = " << bit_ctxt.findBaseLevel() << endl;
				}
			}

			Ctxt tmp(ZeroCtxtLike, *i);
			tmp.addConstant(ZZ(1));
			tmp.addCtxt(*i, true);
			product_heap.store(tmp);

			++i;
			++index;
		}

		//size_t max_mult_levels = 0;

		for (size_t bit = 0; bit < output_size; ++bit) {
			if  (!output_heaps[bit].is_empty()) {
				//max_mult_levels = std::max(max_mult_levels, output_heaps[bit].size());
				output_heaps[bit].extract_result(*(output_first + bit));
				//cout << "output_heaps["<< bit << "].BaseLevel= " << (output_first + bit)->findBaseLevel() << endl;
			}
			else {
				*(output_first + bit) = Ctxt(ZeroCtxtLike, *first);
				//cout << "output_heaps["<< bit << "] is empty"<< endl;
			}
		}

		//cout << "max_mult_levels = " << max_mult_levels << endl;
	}
};


class parallel_folklore_search {
private:
	uint _threads_count;
	std::vector<folklore_search> _folklores;
public:
	parallel_folklore_search(const EncryptedArray& ea, const FHEPubKey& public_key, const FHESecKey& secret_key, uint threads_count = 0) :
		_threads_count(threads_count) {
		if (_threads_count == 0) {
			_threads_count  = get_hardware_thread_count();
		}

		_folklores = std::vector<folklore_search>(_threads_count, folklore_search(ea, public_key, secret_key));
	}

	template<class InputIt, class OutputIt>
	void execute(InputIt first, InputIt last, std::vector<OutputIt> outputs_first) {
		size_t n = std::distance(first, last);
		size_t block_size = n / _threads_count;

		std::vector<std::thread> threads(_threads_count - 1);

		InputIt block_start = first;
		for(size_t t = 0; t < _threads_count - 1; t++) {
			InputIt block_end = block_start;
			std::advance(block_end, block_size);

			threads[t] = std::thread([this, t] (InputIt block_first, InputIt block_last, OutputIt output_first) {
				_folklores[t].execute(block_first, block_last, output_first);
			}, block_start, block_end, outputs_first[t]);

			std::advance(block_start, block_size);
		}

		_folklores[_threads_count - 1].execute(block_start, last, outputs_first[_threads_count - 1]);

		std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));
	}
};



static void args_folklore_search(benchmark::internal::Benchmark* b) {
	for (int input_bit_size = 3; input_bit_size <= 32; ++input_bit_size) {
		//size_t input_size = std::pow(2, input_bit_size); // n
		b->Args({input_bit_size});
	}
}

static void bm_parallel_folklore_search(benchmark::State& state) {
	size_t thread_count = get_hardware_thread_count();
	size_t single_thread_input_size = std::pow(2, state.range(0));
	size_t total_input_size = single_thread_input_size * thread_count;

	size_t level = folklore_search::calc_level_approximation(single_thread_input_size);
	helib_context context(false, level, 2, 1, 80, 3, 1, 128, 500);

	parallel_folklore_search folklore(*context.encrypted_array, *context.public_key, *context.secret_key);

	//prepare input
	std::vector<NewPlaintextArray> plaintext;
	plaintext.reserve(total_input_size);
	for (size_t i = 0; i < total_input_size; i++) {
		NewPlaintextArray plain(*context.encrypted_array);
		long random_bit = rep(random_GF2());
		encode(*context.encrypted_array, plain, random_bit);

		//cout << "[" << i << "]\t\t" << plain << endl;

		plaintext.push_back(plain);
	}


	//find actual first positive index
	std::vector<size_t> actual_first_positive_index(thread_count, 0);
	NewPlaintextArray one_bit(*context.encrypted_array);
	encode(*context.encrypted_array, one_bit, 1);
	for (size_t t = 0; t < thread_count; t++) {
		for (size_t i = 0; i < total_input_size; i++) {
			if (equals(*context.encrypted_array, plaintext[(t * single_thread_input_size)+ i], one_bit)) {
				actual_first_positive_index[t] = i;
				break;
			}
		}
	}

	//std::cout << "\n\nactual_first_positive_index:" << actual_first_positive_index << std::endl;


	bool is_correct = true;
	bool all_ctxts_correct = true;
	for (auto _ : state) {
		state.PauseTiming();
		ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>
			enc_it_begin(*context.public_key, *context.encrypted_array, plaintext.begin());
		ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>
			enc_it_end(*context.public_key, *context.encrypted_array, plaintext.end());

		size_t single_thread_output_size = folklore_search::calc_output_size(single_thread_input_size);
		std::vector<std::vector<Ctxt>> output_vectors(thread_count, std::vector<Ctxt>(single_thread_output_size, Ctxt(*context.public_key)));
		std::vector<std::vector<Ctxt>::iterator> output_iterators;
		output_iterators.reserve(output_vectors.size());

		for (size_t i = 0; i < output_vectors.size(); i++) {
			output_iterators.push_back(output_vectors[i].begin());
		}

		ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>::reset_elapsed_time();
		state.ResumeTiming();
		folklore.execute(enc_it_begin, enc_it_end, output_iterators);
		state.PauseTiming();

		for (size_t t = 0; t < thread_count; t++) {
			size_t local_found_first_positive_index = 0;
			for (size_t i = 0; i < output_vectors[t].size(); i++) {
				vector<long> ptxt;
				context.encrypted_array->decrypt(output_vectors[t][i], *context.secret_key, ptxt);
				//cout <<"output_vectors[" << t << "]["<< i << "]=\t" << ptxt << endl;
				local_found_first_positive_index += (ptxt[0] * std::pow(2, i));

				all_ctxts_correct = all_ctxts_correct && output_vectors[t][i].isCorrect();
			}

			//cout <<"local_found_first_positive_index[" << t << "]=\t" << local_found_first_positive_index << endl;

			is_correct = is_correct && (actual_first_positive_index[t] == local_found_first_positive_index);
		}
	}

	state.counters.insert({
		{"all_ctxts_correct", all_ctxts_correct},
		{"is_correct", is_correct},
		{"total_input_size", total_input_size},
		{"thread_count", thread_count},
		{"helib_level", level},
		{"helib_simd_size", context.encrypted_array->size()},
		{"helib_m", context.param_m},
		{"ad_hoc_enc_ns", ad_hoc_ctxt_iterator<std::vector<NewPlaintextArray>::const_iterator>::get_elapsed_time()}
	});

}


BENCHMARK(bm_parallel_folklore_search)->Apply(args_folklore_search)->MinTime(1e-12)->Repetitions(1);
